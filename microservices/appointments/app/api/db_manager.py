# "stored procedures"
import databases
from api.models import AppointmentIn, AppointmentIssuesIn, ClownsViewClientDetailsIn, RatingIn, StatusIn
from .db import database, Appointments, Appointment_Issues, \
    Clowns_View_Client_Details
from datetime import datetime
import json
from sqlalchemy import select, and_
from pathlib import Path


async def init_populate():
    ''' Initialize the database with dummy data if none is present. '''
    basepath = Path('/')
    files_in_basepath = basepath.iterdir()
    for item in files_in_basepath:
        if item.is_file():
            print(item.name)

    res = await database.fetch_one(query='SELECT True FROM "public"."Appointments" LIMIT 1')
    if not res:
        query = Appointments.insert()
        with open('./app/api/appointments.json') as f:
            values = json.load(f)
        for entry in values:
            entry['date_created'] = datetime.strptime(
                entry['date_created'], '%Y-%m-%d %H:%M:%S')
            entry['date_of_appointment'] = datetime.strptime(
                entry['date_of_appointment'], '%Y-%m-%d %H:%M:%S')
        await database.execute_many(query=query, values=values)

    res = await database.fetch_one(query='SELECT True FROM "public"."Appointment_Issues" LIMIT 1')
    if not res:
        query = Appointment_Issues.insert()
        with open('./app/api/appointment_issues.json') as f:
            values = json.load(f)
        await database.execute_many(query=query, values=values)

    res = await database.fetch_one(
        query='SELECT True FROM "public"."Clowns_View_Client_Details" LIMIT 1')
    if not res:
        query = Clowns_View_Client_Details.insert()
        with open('./app/api/clowns_view_client_details.json') as f:
            values = json.load(f)
        await database.execute_many(query=query, values=values)


async def get_appointment(appointment_id: int):
    ''' Query: retrieve single apointment by appointment_id (any role) '''
    query = Appointments.select(
        Appointments.c.appointment_id == appointment_id)
    return await database.fetch_one(query)


async def get_all_appointments_as_client(client_id: int):
    ''' Query: retrieve all apointments by client_id as client '''
    query = Appointments.select(Appointments.c.client_id == client_id)
    return await database.fetch_all(query)


async def get_upcoming_appointments_as_client(client_id: int):
    ''' Query: retrieve all upcomiong apointments by client_id as client '''
    # Some way (doesn't work):
    # query = Appointments.select(
    #     whereclause='client_id = :client_id AND date_of_appointment >= :now', params={"client_id": client_id, "now": datetime.now()})

    # THE RIGHT WAY DAMNIT XD
    query = Appointments.select(and_(Appointments.c.client_id ==
                                     client_id, Appointments.c.date_of_appointment >= datetime.now()))

    return await database.fetch_all(query)


async def get_past_appointments_as_client(client_id: int):
    ''' Query: retrieve all past apointments by client_id as client '''
    query = Appointments.select(and_(Appointments.c.client_id == client_id,
                                     Appointments.c.date_of_appointment < datetime.now()))
    return await database.fetch_all(query)


async def update_appointment_rating(appointment_id: int, payload: RatingIn):
    ''' Query: update an appointment\'s rating by appointment_id as client '''
    # not using a payload as param as only the id and rating is needed
    query = Appointments.update().where(Appointments.c.appointment_id ==
                                        appointment_id).values(**payload.dict())
    return await database.execute(query)


async def insert_new_appointment(payload: AppointmentIn):
    ''' Query: insert a new appointment by payload as troupe leader '''
    query = Appointments.insert().values(**payload.dict())
    return await database.execute(query)


async def get_check_existing_appointment_date(date_of_appointment: datetime):
    ''' Query: checks whether or not an upcoming appointment with a given datetime already exists. '''
    query = Appointments.select(and_(Appointments.c.date_of_appointment == date_of_appointment,
                                     Appointments.c.status == 'upcoming'))
    return await database.fetch_one(query)


async def get_all_appointments_as_clown(troupe_id: int):
    ''' Query: retrieve all apointments by clown_id as clown '''
    query = Appointments.select(Appointments.c.troupe_id == troupe_id)
    return await database.fetch_all(query)


async def update_appointment_status(appointment_id: int, payload: StatusIn):
    ''' Query: update an appointment\'s status by appointment_id as clown '''
    # not using a payload as param as only the id and rating is needed
    query = Appointments.update().where(Appointments.c.appointment_id ==
                                        appointment_id).values(**payload.dict())
    return await database.execute(query)


async def insert_appointment_issue(payload: AppointmentIssuesIn):
    ''' Query: insert an appointment issue by payload as a clown '''
    query = Appointment_Issues.insert().values(**payload.dict())
    return await database.execute(query)


async def insert_client_details_request(payload: ClownsViewClientDetailsIn):
    ''' Query: insert a request for a client\'s details by payload as clown'''
    query = Clowns_View_Client_Details.insert().values(**payload.dict())
    return await database.execute(query)
