# database model and the tables that queries will be run against; note these have to exist in postgre db already!
import os
from sqlalchemy import (Column, Table, Integer, DateTime, String, MetaData,
                        create_engine)
from databases import Database

DATABASE_URL = os.getenv('DATABASE_URL') or 'postgresql://appointments_admin:tea_leaves*1009@localhost/appointments_db'

engine = create_engine(DATABASE_URL)
metadata = MetaData()

Appointments = Table(
    'Appointments',
    metadata,
    Column('appointment_id', Integer, autoincrement=True, primary_key=True),
    Column('date_created', DateTime),
    Column('date_of_appointment', DateTime),
    Column('client_id', Integer),
    Column('troupe_id', Integer),
    Column('status', String(10)),
    Column('rating', String),
)

Appointment_Issues = Table(
    'Appointment_Issues',
    metadata,
    Column('appointment_issues_id', Integer,
           autoincrement=True, primary_key=True),
    Column('appointment_id', Integer),
    Column('clown_id', Integer),
    Column('title', String(50)),
    Column('description', String(200)),
)

Clowns_View_Client_Details = Table(
    'Clowns_View_Client_Details',
    metadata,
    Column('clowns_view_client_details_id', Integer,
           autoincrement=True, primary_key=True),
    Column('appointment_id', Integer),
    Column('clown_id', Integer),
    Column('reason', String(100)),
)

database = Database(DATABASE_URL)
