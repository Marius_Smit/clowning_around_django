# similiar to django serializers, these translate payloads & responses
from datetime import datetime
from pydantic import BaseModel
from typing import Optional


class AppointmentIn(BaseModel):
    ''' \'Serializer\' for requests with payload corresponding to Appointments table '''
    # even though the date_created is set programmatically when called from app.get, it should still be assignable when cast from database query result!
    date_created: Optional[datetime] = datetime.now()
    date_of_appointment: datetime
    client_id: int
    troupe_id: int
    status: Optional[str] = 'upcoming'
    rating: Optional[str] = ''


class AppointmentOut(AppointmentIn):
    ''' Extends AppointmentIn to append the pk '''
    appointment_id: int


# class AppointmentUpdateRating(AppointmentIn)
# class AppointmentUpdateStatus(AppointmentIn)


class AppointmentIssuesIn(BaseModel):
    ''' \'Serializer\' for requests with payload corresponding to Appointment_Issues table '''
    # appointment_id optional as it can also be retrieved from URL params
    appointment_id: Optional[int]
    clown_id: int
    title: str
    description: str


# class AppointmentIssuesOut(AppointmentIssuesIn):
#     ''' Extends AppointmentIn to append the pk '''
#     appointment_issues_id: int


class ClownsViewClientDetailsIn(BaseModel):
    ''' \'Serializer\' for requests with payload corresponding to Clowns_View_Client_Details table '''
    # appointment_id optional as it can also be retrieved from URL params
    appointment_id: Optional[int]
    clown_id: int
    reason: str


# class ClownsViewClientDetailsOut(ClownsViewClientDetailsIn):
#     ''' Extends ClownsViewClientDetailsIn to append the pk '''
#     clowns_view_client_details_id: int

class IdIn(BaseModel):
    ''' \'Serializer\' for generic bodies with id '''
    id: int


class RatingIn(BaseModel):
    ''' \'Serializer\' for body coming from client's update to an appointment's rating '''
    rating: str


class StatusIn(BaseModel):
    ''' \'Serializer\' for body coming from clown user's request to update appointment status '''
    status: str
