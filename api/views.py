import os
import httpx
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from clowning_around.users.models import Clown, TroupeLeader, Client

APPOINTMENTS_SERVICE_URL = os.environ.get('APPOINTMENTS_SERVICE_URL') or 'http://127.0.0.1:80/api/v1'


@api_view(['GET'])
@permission_classes([IsAuthenticatedOrReadOnly])
def get_appointments_client(request):
    ''' List all appointments as a client. '''
    if request.user.is_client:
        client_id = request.user.pk
        params = {'client_id': client_id}
        r = httpx.get(f'{APPOINTMENTS_SERVICE_URL}/clients/appointments', params=params)
        return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a client and can not access client functions.')


@api_view(['GET'])
@permission_classes([IsAuthenticatedOrReadOnly])
def get_upcoming_appointments_client(request):
    ''' List all upcoming appointments as a client. '''
    if request.user.is_client:
        client_id = request.user.pk
        params = {'client_id': client_id}
        r = httpx.get(f'{APPOINTMENTS_SERVICE_URL}/clients/appointments/upcoming', params=params)
        return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a client and can not access client functions.')


@api_view(['GET'])
@permission_classes([IsAuthenticatedOrReadOnly])
def get_past_appointments_client(request):
    ''' List all past appointments as a client. '''
    if request.user.is_client:
        client_id = request.user.pk
        params = {'client_id': client_id}
        r = httpx.get(f'{APPOINTMENTS_SERVICE_URL}/clients/appointments/past', params=params)
        return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a client and can not access client functions.')


@api_view(['GET', 'PUT'])
@permission_classes([IsAuthenticatedOrReadOnly])
def get_appointment_update_rating_client(request, pk):
    ''' View single appointment and rate as a client. Provide {"rating": "..."} for PUT. '''
    if request.user.is_client:
        if request.method == 'GET':
            r = httpx.get(f'{APPOINTMENTS_SERVICE_URL}/clients/appointments/{pk}')
            return Response(data=r.json(), status=r.status_code)
        else:
            # validation performed in MS
            payload = {"rating": request.data['rating']}
            r = httpx.put(f'{APPOINTMENTS_SERVICE_URL}/clients/appointments/{pk}',
                          json=payload)
            return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a client and can not access client functions.')


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_appointment_troupe_leader(request):
    ''' Add an appointment as a troupe leader. Payload: date_of_appointment, client_id'''
    if request.user.is_troupe_leader:
        troupe_leader_obj_from_db = TroupeLeader.objects.filter(user=request.user).select_related('troupe')[0]
        troupe_id = troupe_leader_obj_from_db.troupe.pk
        request.data['troupe_id'] = troupe_id
        r = httpx.post(f'{APPOINTMENTS_SERVICE_URL}/troupeleader/createappointment', json=request.data)
        return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a troupe leader and can not access troupe leader functions.')


@api_view(['GET'])
@permission_classes([IsAuthenticatedOrReadOnly])
def get_appointments_clown(request):
    ''' List all appointments as a clown. '''
    if request.user.is_clown:
        # providing the troupe_id instead of clown_id as the appointments only have a reference to troupes
        troupe_id = Clown.objects.filter(pk=request.user.pk).select_related('troupe')[0].troupe.pk
        params = {'troupe_id': troupe_id}
        r = httpx.get(f'{APPOINTMENTS_SERVICE_URL}/clowns/appointments', params=params)
        return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a clown and can not access clown functions.')


@api_view(['GET', 'PUT'])
@permission_classes([IsAuthenticatedOrReadOnly])
def get_appointment_update_status_clown(request, pk):
    ''' View single appointment or update status as a clown. Provide {"status": "..."} for PUT. '''
    if request.user.is_clown:
        if request.method == 'GET':
            r = httpx.get(f'{APPOINTMENTS_SERVICE_URL}/clowns/appointments/{pk}')
            return Response(data=r.json(), status=r.status_code)
        else:
            # validation performed in MS
            r = httpx.put(f'{APPOINTMENTS_SERVICE_URL}/clowns/appointments/{pk}', json=request.data)
            return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a client and can not access client functions.')


@api_view(['POST'])
def create_appointment_issue_clown(request, pk):
    ''' Create an appointment issue as a clown. Payload: title, description '''
    if request.user.is_clown:
        clown_id = Clown.objects.filter(user=request.user)[0].pk
        request.data['clown_id'] = clown_id
        r = httpx.post(f'{APPOINTMENTS_SERVICE_URL}/clowns/appointments/{pk}/issue', json=request.data)
        return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a clown and can not access clown functions.')


@api_view(['POST'])
def create_request_for_client_details_clown(request, pk):
    ''' Create a request for client's details as a clown. Payload: reason '''
    if request.user.is_clown:
        clown_id = Clown.objects.filter(user=request.user)[0].pk
        request.data['clown_id'] = clown_id
        r = httpx.post(f'{APPOINTMENTS_SERVICE_URL}/clowns/appointments/{pk}/clientdetails', json=request.data)
        if r.status_code in (201, '201'):
            client_obj_from_db = Client.objects.filter(pk=r.json()['client_id'])[0]
            response = {
                "contact_name": client_obj_from_db.contact_name,
                "contact_email": client_obj_from_db.contact_email,
                "contact_number": client_obj_from_db.contact_number,
            }
            return Response(data=response, status=201)
        # if there was an error, return message from MS:
        return Response(data=r.json(), status=r.status_code)
    else:
        raise PermissionError('You are not a clown and can not access clown functions.')
