from django.urls import path
from . import views

urlpatterns = [
    # clients:
    path('clients/appointments', views.get_appointments_client),
    path('clients/appointments/<int:pk>', views.get_appointment_update_rating_client),
    path('clients/appointments/upcoming', views.get_upcoming_appointments_client),
    path('clients/appointments/past', views.get_past_appointments_client),

    # troupe leaders:
    path('troupeleader/createappointment', views.create_appointment_troupe_leader),

    # clowns:
    path('clowns/appointments', views.get_appointments_clown),
    path('clowns/appointments/<int:pk>', views.get_appointment_update_status_clown),
    path('clowns/appointments/<int:pk>/issue', views.create_appointment_issue_clown),
    path('clowns/appointments/<int:pk>/clientdetails', views.create_request_for_client_details_clown),
]
