from fastapi import FastAPI
from .api.appointments import appointments
from .api.db import metadata, database, engine
from .api.db_manager import init_populate

metadata.create_all(engine)

app = FastAPI(openapi_url="/api/v1/appointments/openapi.json",
              docs_url="/api/v1/appointments/docs")


@app.on_event('startup')
async def startup():
    await database.connect()
    await init_populate()


@app.on_event('shutdown')
async def shutdown():
    await database.disconnect()

app.include_router(appointments, prefix='/api/v1', tags=['appointments'])
